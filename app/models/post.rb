class Post < ApplicationRecord
  validates_presence_of :title, :categories, :content
end
